package cmd

import (
	"fmt"
	"os"
)

func parseIssue(c *command) error {
	c.name = os.Args[0]
	c.description = "description"
	c.args = os.Args[2:]
	fmt.Println("There are", len(c.args), "issue args:\n", c.args)
	return nil
}
