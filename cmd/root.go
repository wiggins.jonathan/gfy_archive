package cmd

import (
	"fmt"
	"os"
)

type command struct {
	name        string
	description string
	args        []string
	subcommands []*command
}

func Parse() *command {
	var c command

	// If no args passed to gfy, print default usage
	if len(os.Args) < 2 {
		c.usage()
	}

	// Dispatch based on first arg passed into gfy
	var err error
	switch os.Args[1] {
	case "issue":
		err = parseIssue(&c)
	case "review":
		err = parseReview(&c)
	case "milestone":
		err = parseMilestone(&c)
	default:
		c.usage()
	}

	if err != nil {
		c.usage()
	}

	return &c
}

// This function prints usage to stdout
func (c *command) usage() {
	//We might want to store this entire thing as a function in the command object?
	message := Colorize("red", `gfy is a companion tool for git. A Git For You.

Usage: <command> <subcommands> <args>`)
	fmt.Println(message)
	os.Exit(0)
}

// Colorizes strings for terminal output using ANSI color codes
func Colorize(color, message string) string {

	reset := "\u001b[0m"
	switch color {
	case "black":
		color = "\u001b[30m"
	case "red":
		color = "\u001b[31m"
	case "green":
		color = "\u001b[32m"
	case "yellow":
		color = "\u001b[33m"
	case "blue":
		color = "\u001b[34m"
	case "magenta":
		color = "\u001b[35m"
	case "cyan":
		color = "\u001b[36m"
	case "white":
		color = "\u001b[37m"
	}

	return color + message + reset
}
