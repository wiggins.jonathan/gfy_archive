package cmd

import (
	"fmt"
	"os"
)

func parseReview(c *command) error {
	c.name = os.Args[0]
	c.description = "description"
	c.args = os.Args[2:]
	fmt.Println("There are", len(c.args), "review args:\n", c.args)
	return nil
}
